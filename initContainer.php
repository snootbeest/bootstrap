<?php declare(strict_types=1);

$definitions = require_once('config.php');

use DI\ContainerBuilder;

$builder = new ContainerBuilder();
$builder->useAutowiring(true);
$builder->useAnnotations(true);
$builder->addDefinitions($definitions);
$container = $builder->build();

return $container;
