<?php declare(strict_types=1); 

/** @var \DI\Container $container */
$container = require_once('initContainer.php');
return $container->get('db.params');