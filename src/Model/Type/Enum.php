<?php declare(strict_types=1);

namespace ExampleNamespace\Model\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

abstract class Enum extends Type
{
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        $enumString = implode('", "', static::VALUES);
        return sprintf('ENUM("%s")', $enumString);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if (!in_array($value, static::VALUES)) {
            throw new \InvalidArgumentException(sprintf('Invalid value for %s', static::NAME));
        }
        return $value;
    }

    public function getName()
    {
        return static::NAME;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}