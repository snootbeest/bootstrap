<?php declare(strict_types=1);

namespace ExampleNamespace\Model;

use Doctrine\ORM\EntityRepository;

class Repository extends EntityRepository
{
    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }
}