<?php declare(strict_types=1);

namespace ExampleNamespace\Model;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Doctrine\UuidGenerator;

abstract class Entity
{
    /**
     * @var string $id
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    protected $id;

    /**
     * Get the id
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }
}