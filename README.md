# Bootstrap
This project provides a boilerplate slim4 / doctrine + mariadb project.

## Requirements
* [docker](https://www.docker.com/)
* [docker-compose](https://docs.docker.com/compose/)

## Quick Start
* Copy this code into your project directory
* Search for and replace `ExampleNamespace` with your own namespace.
* Run `docker-compose up --build`

Once the containers have finished building, you should be able to navigate to `localhost`