<?php declare(strict_types=1);

use DI\Bridge\Slim\Bridge;

$container = require_once('initContainer.php');
return Bridge::create($container);
