<?php declare(strict_types=1);

require_once('vendor/autoload.php');

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;

return [
    'environment'         => DI\env('ENV', 'dev'),
    'path.root'           => realpath(__DIR__),
    'db.params' => [
        'driver'   => 'pdo_mysql',
        'host'     => 'mariadb',
        'user'     => 'root',
        'password' => DI\env('MYSQL_ROOT_PASSWORD'),
        'dbname'   => DI\env('MARIADB_DATABASE'),
    ],
    EntityManager::class => DI\factory([EntityManager::class, 'create'])
        ->parameter('connection', DI\get('db.params'))
        ->parameter('config', DI\get(Configuration::class)),
    Configuration::class => function (ContainerInterface $container) {
        $rootPath = $container->get('path.root');
        $devMode = $container->get('environment') === 'dev';

        // todo: figure out query & result caching
        $config = new Configuration();
        $config->setMetadataDriverImpl($config->newDefaultAnnotationDriver(sprintf('%s/%s', $rootPath, 'src/Model'), false));
        $config->setProxyDir(sprintf('%s/%s', $rootPath, 'cache/doctrine'));
        $config->setProxyNamespace('PHPBot\DoctrineProxies');
        if ($devMode === true) {
            $config->setAutoGenerateProxyClasses(true);
        } else {
            $config->setAutoGenerateProxyClasses(false);
        }

        $entityManager = EntityManager::create($container->get('db.params'), $config);
        $platform = $entityManager->getConnection()->getDatabasePlatform();
        $platform->registerDoctrineTypeMapping('enum', 'string');

        Type::addType('uuid', \Ramsey\Uuid\Doctrine\UuidType::class);
        Type::addType(AccessTokenType::NAME, AccessTokenType::class);

        return $config;
    },
];